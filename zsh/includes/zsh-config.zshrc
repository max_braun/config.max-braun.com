setopt auto_cd
setopt auto_pushd
setopt no_beep
setopt cd_able_vars
setopt append_history
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_verify
setopt extended_history
setopt hist_save_no_dups
setopt hist_expire_dups_first
setopt hist_find_no_dups
setopt correct

export IFS=$'\n'
